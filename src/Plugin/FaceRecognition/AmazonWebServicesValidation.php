<?php

namespace Drupal\face_recognition_aws\Plugin\FaceRecognition;

use Aws\Credentials\Credentials;
use Aws\Rekognition\RekognitionClient;
use Drupal\Core\Form\FormStateInterface;
use Drupal\face_recognition\FaceRecognitionPluginBase;
use Drupal\Core\Utility\Error;

/**
 * Plugin implementation of the face_recognition.
 *
 * @FaceRecognition(
 *   id = "aws_validation",
 *   label = @Translation("Amazon Web Services Validation"),
 *   description = @Translation("Based on AWS Rekognition.")
 * )
 */
class AmazonWebServicesValidation extends FaceRecognitionPluginBase {

  /**
   * Validate with AWS Rekognition.
   */
  public function validate() {

    $credentials = new Credentials($this->configuration['aws_key'], $this->configuration['aws_secret']);

    $options = [
      'region' => $this->configuration['aws_region'],
      'version' => $this->configuration['aws_version'],
      'credentials' => $credentials,
    ];

    try {
      $client = new RekognitionClient($options);
      $result = $client->compareFaces([
        'SimilarityThreshold' => 90,
        'SourceImage' => [
          'Bytes' => file_get_contents($this->inputPicture),
        ],
        'TargetImage' => [
          'Bytes' => file_get_contents($this->referencePicture),
        ],
      ]);
      $this->logger->get('face_recognition')->info('AWS request response: @result', ['@result' => print_r($result, TRUE)]);
      if (count($result['FaceMatches']) == 0) {
        $this->messenger->addError($this->t('The face can not be detected'));
      }
      elseif ($result['FaceMatches'][0]['Similarity'] >= $this->configuration['aws_threshold']) {
        return TRUE;
      }
      else {
        $this->messenger->addError($this->t('The face can not be recognised'));
        return FALSE;
      }
    }
    catch (\Exception $e) {
      $variables = Error::decodeException($e);
      $this->logger->get('face_recognition_aws')->error('%type: @message in %function (line %line of %file).', $variables);
    }
  }

  /**
   * Adds some plugin configuration in order to make it work.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form[$this->getPluginId()]['aws_region'] = [
      '#type' => 'textfield',
      '#size' => 20,
      '#title' => $this->t('Region'),
      '#description' => $this->t('eu-west-1'),
    ];
    $form[$this->getPluginId()]['aws_version'] = [
      '#type' => 'textfield',
      '#size' => 20,
      '#title' => $this->t('Version'),
      '#description' => $this->t('2016-06-27'),
    ];
    $form[$this->getPluginId()]['aws_key'] = [
      '#type' => 'textfield',
      '#size' => 20,
      '#title' => $this->t('AWS Key'),
      '#description' => $this->t('Configuration from your AWS Account.'),
    ];
    $form[$this->getPluginId()]['aws_secret'] = [
      '#type' => 'textfield',
      '#size' => 40,
      '#title' => $this->t('AWS Secret'),
      '#description' => $this->t('Configuration from your AWS Account.'),
    ];
    $form[$this->getPluginId()]['aws_threshold'] = [
      '#type' => 'textfield',
      '#size' => 3,
      '#title' => $this->t('Similarity Threshold'),
      '#description' => $this->t('For instance: 90'),
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

}
